dnl Check for system tomsfastmath
PKG_CHECK_MODULES([TOMSFASTMATH], [tomsfastmath], [have_system_tomsfastmath=yes], [have_system_tomsfastmath=no])

AM_CONDITIONAL([SYSTEM_TOMSFASTMATH], [test "x$have_system_tomsfastmath" = "xyes"])

if test "x$have_system_tomsfastmath" = "xyes"; then
    AC_DEFINE([HAVE_SYSTEM_TOMSFASTMATH], [1], [link against system-wide tomsfastmath library])
    tomsfastmath_msg="External, $TOMSFASTMATH_CFLAGS $TOMSFASTMATH_LIBS"
else
    AC_DEFINE([HAVE_SYSTEM_TOMSFASTMATH], [0], [don't link against system-wide tomsfastmath library])
    tomsfastmath_msg="Internal"
fi
